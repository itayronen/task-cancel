# task-cancel
Cancellation mechanism, written in typescript.

# Install
`npm install task-cancel`

# Usage
```ts
suite.test("When cancelled, isCancelRequested is true.", test => {
	test.arrange();
	let source = new CancelTokenSource();
	source.cancel();

	test.assert();
	assert.equal(source.token.isCancelRequested, true);
});

suite.test("When cancelled, onCancel is triggered.", test => {
	test.arrange();
	let source = new CancelTokenSource();
	let isTriggered = false;
	source.token.onCancel.add(() => isTriggered = true);

	test.act();
	source.cancel();

	test.assert();
	assert.equal(isTriggered, true);
});

suite.test("When link cancelled, isCancelRequested is true.", test => {
	test.arrange();
	let parent = new CancelTokenSource();
	let source = new CancelTokenSource([parent.token]);

	test.act();
	parent.cancel();

	test.assert();
	assert.equal(source.token.isCancelRequested, true);
});
```
import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";
import { CancelTokenSource, CancelTokenSourceState } from "./CancelTokenSource";

export default function (suite: TestSuite): void {
	suite.describe(CancelTokenSource, suite => {

		suite.describe("Simple", suite => {

			suite.test("When not cancelled, isCancelRequested is false.", test => {
				test.arrange();
				let source = new CancelTokenSource();

				test.assert();
				assert.equal(source.token.isCancelRequested, false);
			});

			suite.test("When cancelled, isCancelRequested is true.", test => {
				test.arrange();
				let source = new CancelTokenSource();
				source.cancel();

				test.assert();
				assert.equal(source.token.isCancelRequested, true);
			});

			suite.test("When cancelled, onCancel is triggered.", test => {
				test.arrange();
				let source = new CancelTokenSource();
				let isTriggered = false;
				source.token.onCancel.add(() => isTriggered = true);

				test.act();
				source.cancel();

				test.assert();
				assert.equal(isTriggered, true);
			});
		});

		suite.describe("Linked", suite => {
			suite.test("When link not triggered, isCancelRequested is false.", test => {
				test.arrange();
				let parent = new CancelTokenSource();
				let source = new CancelTokenSource([parent.token]);

				test.assert();
				assert.equal(source.token.isCancelRequested, false);
			});

			suite.test("When link cancelled, isCancelRequested is true.", test => {
				test.arrange();
				let parent = new CancelTokenSource();
				let source = new CancelTokenSource([parent.token]);

				test.act();
				parent.cancel();

				test.assert();
				assert.equal(source.token.isCancelRequested, true);
			});

			suite.test("When link cancelled, onCancel is triggered.", test => {
				test.arrange();
				let parent = new CancelTokenSource();
				let source = new CancelTokenSource([parent.token]);
				let isTriggered = false;
				source.token.onCancel.add(() => isTriggered = true);

				test.act();
				parent.cancel();

				test.assert();
				assert.equal(isTriggered, true);
			});

			suite.test("When link cancelled, onCancel is triggered after isCancelRequested has propegated to all dependents.", test => {
				test.arrange();
				let parent = new CancelTokenSource();
				let child1 = new CancelTokenSource([parent.token]);
				let child2 = new CancelTokenSource([parent.token]);
				let child2IsCancelRequested = false;
				child1.token.onCancel.add(() => child2IsCancelRequested = child2.token.isCancelRequested);

				test.act();
				parent.cancel();

				test.assert();
				assert.equal(child2IsCancelRequested, true);
			});

			suite.test("When multiple links are cancelled, then source is cancelled.", test => {
				test.arrange();
				let dependency1 = new CancelTokenSource();
				let dependency2 = new CancelTokenSource();
				let source = new CancelTokenSource([dependency1.token, dependency2.token]);

				test.act();
				dependency1.cancel();
				dependency2.cancel();

				test.assert();
				assert.equal(source.token.isCancelRequested, true);
			});

			suite.test("When source is closed then a link is cancelled, "
				+ "then source is closed and token is not changed.", test => {
					test.arrange();
					let dependency = new CancelTokenSource();
					let source = new CancelTokenSource([dependency.token]);
					let isTriggered = false;
					source.token.onCancel.addOnce(() => isTriggered = true);

					test.act();
					source.close();
					dependency.cancel();

					test.assert();
					assert.equal(source.state, CancelTokenSourceState.Closed);
					assert.equal(source.token.isCancelRequested, false);
					assert.equal(isTriggered, false);
				});
		});
	});
}
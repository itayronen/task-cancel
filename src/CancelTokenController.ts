export interface CancelTokenController{
	setStateCancelled: () => void;
	triggerOnCancel: () => void
}
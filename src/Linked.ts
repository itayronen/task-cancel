export var LinkSymbol = Symbol("Linked cancellation");

export interface Linked {
	setStateCancelled(): void;
}
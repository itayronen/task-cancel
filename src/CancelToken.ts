import { Event, Event1 } from "itay-events";
import { CancelError } from "./CancelError";
import { CancelTokenController } from "./CancelTokenController";
import { Linked, LinkSymbol } from "./Linked";

export class CancelToken {
	public static readonly none = new CancelToken({} as CancelTokenController);

	private _isCancelRequested: boolean = false;
	private _onCancel = new Event();

	private links: Linked[] | undefined;

	constructor(controller: CancelTokenController) {
		controller.setStateCancelled = () => this.setStateCancelled();
		controller.triggerOnCancel = () => this._onCancel.trigger();
	}

	public static fromPromise(promise: Promise<void>): CancelToken {
		let controller: CancelTokenController = {} as CancelTokenController;
		let token = new CancelToken(controller);

		let cancel = () => {
			controller.setStateCancelled();
			controller.triggerOnCancel();
		};

		promise.then(() => cancel(), () => cancel());

		return token;
	}

	public get isCancelRequested(): boolean { return this._isCancelRequested; }

	public get onCancel(): Event { return this._onCancel; }

	public throwIfCancelRequested(): void {
		if (this.isCancelRequested) {
			throw new CancelError();
		}
	}

	public [LinkSymbol](linked: Linked): void {
		if (!this.links) this.links = [];

		this.links.push(linked);
	}

	public get [Symbol.toStringTag]() {
		return 'CancelToken';
	}

	private setStateCancelled(): void {
		if (this._isCancelRequested) return;

		this._isCancelRequested = true;

		if (this.links) {
			for (let linked of this.links) {
				linked.setStateCancelled();
			}

			delete this.links;
		}
	}
}
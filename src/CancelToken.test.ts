import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { CancelToken } from "./CancelToken";

export default function (suite: TestSuite): void {
	suite.describe(CancelToken, suite => {
		suite.describe(CancelToken.fromPromise, suite => {
			suite.test("When promise is running then resolved, then token is cancelled.", async test => {
				test.arrange();
				let promise = new Promise<void>(r => setTimeout(r, 1));
				let token = CancelToken.fromPromise(promise);

				expect(token.isCancelRequested).to.be.false;

				test.act();
				await promise;

				test.assert();
				expect(token.isCancelRequested).to.be.true;
			});

			suite.test("When promise is already resolved, then token is cancelled.", async test => {
				test.act();
				let token = CancelToken.fromPromise(Promise.resolve());

				await new Promise(r => setTimeout(r, 1));

				test.assert();
				expect(token.isCancelRequested).to.be.true;
			});

			suite.test("When promise is already rejected, then token is cancelled.", async test => {
				test.act();
				let token = CancelToken.fromPromise(Promise.reject("error"));

				await new Promise(r => setTimeout(r, 1));

				test.assert();
				expect(token.isCancelRequested).to.be.true;
			});
		});
	});
}
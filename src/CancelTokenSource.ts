import { CancelToken } from "./CancelToken";
import { Linked, LinkSymbol } from "./Linked";
import { CancelTokenController } from "./CancelTokenController";

enum State {
	Opened,
	CancelRequested,
	Closed
}

export { State as CancelTokenSourceState };

export class CancelTokenSource {
	private _state = State.Opened;
	private _token: CancelToken;
	private controller: CancelTokenController;

	constructor(private dependencies?: CancelToken[]) {
		this.controller = {} as CancelTokenController;
		this._token = new CancelToken(this.controller);

		this.registerToDependencies();
	}

	public get state(): State {
		return this._state;
	}

	public get token(): CancelToken {
		return this._token;
	}

	public cancel(): void {
		if (this._state != State.Opened)
			throw new Error(`Cannot call ${this.cancel.name} when state is not ${State[State.Opened]}.`);

		this._state = State.CancelRequested;
		this.controller.setStateCancelled();
		this.controller.triggerOnCancel();
	}

	public close(): void {
		if (this._state != State.Opened)
			throw new Error(`Cannot call ${this.close.name} when state is not ${State[State.Opened]}.`);

		this._state = State.Closed;
	}

	private registerToDependencies(): void {
		if (!this.dependencies || this.dependencies.length === 0) return;

		let linked: Linked = { setStateCancelled: () => this.handleDependencyStateCancel() };

		for (let token of this.dependencies) {
			(token as any)[LinkSymbol](linked);
			token.onCancel.addOnce(this.handleDependencyOnCancel);
		}
	}

	private handleDependencyStateCancel(): void {
		if (this._state !== State.Opened) return;

		this._state = State.CancelRequested;
		this.controller.setStateCancelled();
	}

	private handleDependencyOnCancel = () => {
		if (this._state === State.Closed) return;

		this.controller.triggerOnCancel();
	}
}